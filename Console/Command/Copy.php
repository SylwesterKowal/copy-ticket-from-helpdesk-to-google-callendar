<?php


namespace Kowal\CopyTicketToCallendar\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Copy extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\CopyTicketToCallendar\lib\Execute $execute,
        $name = null
    )
    {
        $this->execute = $execute;
        parent::__construct($name);
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);

        $this->execute->start();

    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_copytickettocallendar:copy");
        $this->setDescription("Copy Ticket to Google Callendar");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}
