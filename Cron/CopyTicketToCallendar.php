<?php


namespace Kowal\CopyTicketToCallendar\Cron;

use Kowal\CopyTicketToCallendar\lib\Queries;
use Psr\Log\LoggerInterface;

class CopyTicketToCallendar
{

    protected $logger;

    /**
     * CopyTicketToCallendar constructor.
     * @param LoggerInterface $logger
     * @param Queries $queries
     * @param SaveEvent $saveEvent
     */
    public function __construct(
        LoggerInterface $logger,
        Queries $queries,
        SaveEvent $saveEvent
    )
    {
        $this->logger = $logger;
        $this->queries = $queries;
        $this->saveEvent = $saveEvent;
    }


    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {

        $tickets = $this->queries->getAllTicketsToCallendar();
        foreach ($tickets as $ticket) {
            if ($eventId = $this->saveEvent->execute($ticket)) {
                $this->queries->setEventIdForTicket($ticket['ticket_id'], $eventId);
                $this->logger->addInfo("ticket_id: " . $ticket['ticket_id']);
            }
        }

    }
}