<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-09-25
 * Time: 11:56
 */

namespace Kowal\CopyTicketToCallendar\lib;

use Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar;

/**
 * Class Save
 */
class SaveEvent
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    private $timezone;

    /**
     * @var GoogleCalendar
     */
    private $googleCalendar;


    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper
     */
    private $eventMapper;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ReminderPersistence
     */
    private $reminderPersistence;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var
     */
    private $ticket;

    /**
     * SaveEvent constructor.
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $timezone
     * @param GoogleCalendar $googleCalendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\ReminderPersistence $reminderPersistence
     * @param \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper
     * @param Queries $queries
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        GoogleCalendar $googleCalendar,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\ReminderPersistence $reminderPersistence,
        \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper,
        Queries $queries
    )
    {
        $this->googleCalendar = $googleCalendar;
        $this->timezone = $timezone;
        $this->eventMapper = $eventMapper;
        $this->reminderPersistence = $reminderPersistence;
        $this->user = $user;
        $this->queries = $queries;
    }

    /**
     * @param $ticket
     */
    public function execute($ticket)
    {
        $this->ticket = $ticket;

        try {
            $this->user->setUserId($this->ticket['user_id']);
            if ($calendar = $this->user->getUserCalendarByIdCustom($this->getCalendarId())) {

                $googleEvent = $this->getGoogleCalendarEvent();
                $event = $this->googleCalendar->saveEvent($googleEvent, $calendar->getGoogleId());
                $eventId = $event->getId();
                if ($eventId) {
                    return $eventId;
                } else {
                    return false;
                }
            } else {
                return null;
            }
            return __('No data saved');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $e->getMessage();
        } catch (\Exception $e) {
            return __('Something went wrong.');
        }


    }

    /**
     * @return \Google_Service_Calendar_Event
     */
    private function getGoogleCalendarEvent()
    {
        $googleEvent = new \Google_Service_Calendar_Event();

        $googleEvent->setSummary($this->getSummary());
        $googleEvent->setLocation($this->getLocation());
        $googleEvent->setDescription($this->getDescription());
        $googleEvent->setColorId($this->getColorId());
        $googleEvent->setVisibility($this->getVisibility());
        $googleEvent->setTransparency($this->getTransparency());


        $getDate = function (\Google_Service_Calendar_EventDateTime $eventDate, $date) {
            if (strpos($date, 'T') === false) {
                $eventDate->setDate($date);
            } else {
                $eventDate->setDateTime($date);
            }
            return $eventDate;
        };
        $googleEvent->setStart($getDate(new \Google_Service_Calendar_EventDateTime(), $this->getStart()));
        $googleEvent->setEnd($getDate(new \Google_Service_Calendar_EventDateTime(), $this->getEnd()));


        return $googleEvent;
    }

    private function getCalendarId()
    {
        return $this->queries->getCalendarIdByUserId($this->ticket['user_id']);
    }

    private function getEventId()
    {
        return $this->getRequest()->getParam('event_id');
    }

    private function getSummary()
    {
        return empty($this->ticket['subject']) ? null : $this->ticket['subject'];
    }

    private function getDescription()
    {
        return null; // na chwilę obecną nie ma pola opis
    }

    private function getLocation()
    {
        return empty($this->ticket['f_location']) ? null : $this->ticket['f_location'];
    }

    private function getColorId()
    {
        return empty($this->ticket['priority_id']) ? null : (int)$this->ticket['priority_id'];
    }

    private function getRecurrence()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['recurrence']) ? false : (bool)$params['recurrence'];
    }

    private function getVisibility()
    {

        return 'default';
    }

    private function getTransparency()
    {
        return 'opaque';
    }

    private function getStart()
    {

        return $this->normalizeDateTime($this->ticket['f_data_rozpoczecia'], $this->ticket['f_data_rozpoczecia']);
    }

    private function getEnd()
    {
        return $this->normalizeDateTime($this->ticket['f_data_zakonczenia'], $this->ticket['f_data_zakonczenia'], '+1 days');
    }

    private function normalizeDateTime($date, $time, $modify = null)
    {
        $date = new \DateTimeImmutable($date, new \DateTimeZone($this->timezone->getConfigTimezone()));

        return $modify ? $date->modify($modify)->format('Y-m-d') : $date->format('Y-m-d');

    }

    /**
     * @return \Google_Service_Calendar_EventAttendee[]
     */
    private function getAttendees()
    {
        $guests = $this->getRequest()->getParam('guests', []);
        $attendees = [];
        if (!empty($guests['guest_emails'])) {
            $emails = explode(',', $guests['guest_emails']);
            foreach ($emails as $email) {
                $attendee = new \Google_Service_Calendar_EventAttendee();
                $email = trim($email);
                preg_match('/^([^<>]+) ?<(.+@.+\..+)>$/', $email, $matches);
                $emailMatch = null;
                if (count($matches) === 3) {
                    $nameMatch = trim($matches[1]);
                    $emailMatch = trim($matches[2]);
                    $attendee->setDisplayName($nameMatch);
                } elseif (preg_match('/^.+@.+\..+$/', $email)) {
                    $emailMatch = $email;
                }
                if ($emailMatch) {
                    $attendee->setEmail($emailMatch);
                    $attendees[] = $attendee;
                }
            }
        }
        return $attendees;
    }

    private function getGuestsCanModify()
    {
        $guests = $this->getRequest()->getParam('guests', []);

        return empty($guests['guests_modify_event']) ? false : (bool)$guests['guests_modify_event'];
    }

    private function getGuestsCanInviteOthers()
    {
        $guests = $this->getRequest()->getParam('guests', []);

        return empty($guests['guests_invite_others']) ? false : (bool)$guests['guests_invite_others'];
    }

    private function getGuestsCanSeeOtherGuests()
    {
        $guests = $this->getRequest()->getParam('guests', []);

        return empty($guests['guests_see_guest_list']) ? false : (bool)$guests['guests_see_guest_list'];
    }

    /**
     * @return array
     */
    protected function getReminders()
    {
        $reminderSettings = $this->getRequest()->getParam('reminders', []);

        return isset($reminderSettings['reminders']) ? $reminderSettings['reminders'] : [];
    }

    /**
     * @return array
     */
    protected function getTagIds()
    {
        $tagIds = [];
        $mainData = $this->getRequest()->getParam('general', []);
        if (!empty($mainData['data']['tags'])) {
            $tagIds = array_map('intval', $mainData['data']['tags']);
            $tagIds = array_filter($tagIds);
        }

        return $tagIds;
    }

    /**
     * @param string $eventId
     * @param int $calendarId
     * @return array
     */
    protected function getRedirectParams($eventId, $calendarId)
    {
        $alias = $this->getRequest()->getParam('back_alias');
        switch ($alias) {
            case 'new':
                return ['*/*/edit'];
            case 'close':
                return ['*/*/'];
            default:
                return ['*/*/edit', ['calendar' => $calendarId, 'event' => $eventId]];
        }
    }
}
