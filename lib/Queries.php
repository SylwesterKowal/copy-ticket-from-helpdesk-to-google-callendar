<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 14/11/2018
 * Time: 02:08
 */

namespace Kowal\CopyTicketToCallendar\lib;


class Queries
{
    protected $_tablePrefix = '';

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {

        $this->connection = $resourceConnection->getConnection();
    }

    public
    function _applyTablePrefix($query)
    {
        return str_replace('PFX_', $this->_tablePrefix, $query);
    }

    public function getAllTicketsToCallendar($tabel = 'mst_helpdesk_ticket')
    {
        $query = "SELECT * FROM PFX_" . $tabel . " WHERE f_kalendarz = :f_kalendarz AND f_event_id IS NULL OR f_kalendarz = :f_kalendarz AND f_event_id = ''";
        $query = $this->_applyTablePrefix($query);
        return $this->connection->fetchAll($query, array('f_kalendarz' => 1));
    }

    public function getCalendarIdByUserId($user_id, $tabel = 'celesta_calendar_calendar')
    {
        $query = "SELECT calendar_id FROM PFX_" . $tabel . " WHERE user_id = :user_id AND is_primary = :is_primary AND is_connected = :is_connected";
        $query = $this->_applyTablePrefix($query);
        return $this->connection->fetchOne($query, array('user_id' => $user_id, 'is_primary' => 1, 'is_connected' => 1));
    }

    /**
     * @param $ticket_id
     * @param $event_id
     * @param string $tabel
     * @return mixed
     */
    public function setEventIdForTicket($ticket_id, $event_id, $tabel = 'mst_helpdesk_ticket')
    {
        $query = "UPDATE PFX_" . $tabel . " SET f_event_id = :event_id WHERE ticket_id = :ticket_id";
        $query = $this->_applyTablePrefix($query);
        return $this->connection->query($query, array('event_id' => $event_id, 'ticket_id' => $ticket_id));
    }
}