<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-09-25
 * Time: 11:43
 */

namespace Kowal\CopyTicketToCallendar\lib;


class Execute
{
    public function __construct(
        Queries $queries,
        SaveEvent $saveEvent
    )
    {
        $this->queries = $queries;
        $this->saveEvent = $saveEvent;
    }

    public function start()
    {
        $tickets = $this->queries->getAllTicketsToCallendar();
        foreach ($tickets as $ticket) {
            if ($eventId = $this->saveEvent->execute($ticket)) {
                $this->queries->setEventIdForTicket($ticket['ticket_id'], $eventId);
                echo "ticket_id: " . $ticket['ticket_id'] . "\n";
            }
        }
    }
}